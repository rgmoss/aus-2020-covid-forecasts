# Australian COVID-19 forecasts: May to October 2020

This repository contains the forecasting model code and input data files necessary to reproduce the outputs presented in the manuscript "[Forecasting COVID-19 activity in Australia to support pandemic response: May to October 2020](https://doi.org/10.1038/s41598-023-35668-6)", *Scientific Reports* 13, 8763 (2023).

## License

The code is distributed under the terms of the BSD 3-Clause license (see `LICENSE`).

## Usage

Forecast outputs are stored as `.rds` files in the `forecast_outputs/` directory.

Manuscript figures and tables are stored in the `manuscript_files/` directory.

See the installation instructions (below) to ensure that you have all of the necessary software installed.

### Generate manuscript figures and tables

You can regenerate the manuscript files by running:

    ./generate_manuscript_files.R

### Regenerate forecast outputs

**NOTE:** this may take several hours.

1. Download the forecast inputs for each data date ([doi:10.26188/19315055](https://doi.org/10.26188/19315055)), and store each `input_2020-MM-DD.tar.bz2` file in the `archives/` directory.

2. Run the forecast simulations for each data date:

   ```sh
   . ./venv/bin/activate
   ./run_past_forecasts.py
   ```

3. Remove the `.rds` files in the `forecast_outputs/` directory:

   ```sh
   rm ./forecast_outputs/*.rds
   ```

4. Regenerate the `.rds` files and manuscript files:

   ```sh
   ./generate_manuscript_files.R --refresh-data
   ```

## Installation: Python

Running the forecast simulations requires Python >= 3.7.
Create a Python virtual environment for running the forecast simulations:

```sh
virtualenv -p python3 venv
. ./venv/bin/activate
pip install -r requirements.txt
```

## Installation: R

Generating the manuscript files requires R >= 4.0 and the following packages:

- [tidyverse](https://www.tidyverse.org/)
- [rhdf5](https://bioconductor.org/packages/rhdf5)
- [scoringRules](https://github.com/FK83/scoringRules)
- [scales](https://scales.r-lib.org/)
- [zoo](https://zoo.r-forge.r-project.org/)

You can install these packages from within R:

```R
install.packages(c('tidyverse', 'scoringRules', 'scales', 'zoo'))
if (!require("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install("rhdf5")
```
