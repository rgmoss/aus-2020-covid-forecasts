#!/usr/bin/env python
"""
Run forecasts for any number of previous data sets.
"""

import argparse
import datetime
import glob
import epifx
import epifx.cmd.forecast
import h5py
import multiprocessing
import numpy as np
import os
import os.path
import re
import sys
import tarfile
import tomli


def parser(default_config_file):
    p = argparse.ArgumentParser(
        description='Run retrospective forecasts for previous data dates.')
    p.add_argument(
        'config_file',
        metavar='config.toml',
        default=default_config_file,
        nargs='?',
        help='The configuration file (default: {})'.format(
            default_config_file))

    return p


def main(args=None):
    p = parser('run_past_forecasts.toml')
    args = p.parse_args(args)
    toml_file = args.config_file
    config = load_config(toml_file)

    # Run all of the forecasts.
    run_all_forecasts(config)

    return 0


def load_config(toml_file):
    with open(toml_file, 'rb') as f:
        config = tomli.load(f)

    # Validation: ensure required tables are provided.
    req_tables = ['settings']
    for t in req_tables:
        if t not in config:
            raise ValueError('Table missing: {}'.format(t))

    # Validation: ensure required settings are provided.
    req_setting = [
        'data_root_dir', 'output_dir', 'output_file', 'toml_files',
        'jurisdictions', 'keep_forecast_files',
    ]
    for r in req_setting:
        if r not in config['settings']:
            raise ValueError('Setting missing: {}'.format(r))

    return config


def get_forecast_dates(config, date_fmt='%Y-%m-%d'):
    data_root_dir = config['settings']['data_root_dir']
    forecast_dates = []

    with os.scandir(data_root_dir) as files:
        for entry in files:
            if entry.is_dir():
                try:
                    _ = datetime.datetime.strptime(entry.name, date_fmt)
                    forecast_dates.append(entry.name)
                except ValueError:
                    pass

    return forecast_dates


def clean_output_dir(output_dir, verbose=True):
    if not os.path.isdir(output_dir):
        if verbose:
            print('Creating {} ...'.format(output_dir))
        os.mkdir(output_dir)

    pattern = os.path.join(output_dir, '*.hdf5')
    files = sorted(glob.glob(pattern))

    for f in files:
        if verbose:
            print('Deleting {} ...'.format(f))
        os.remove(f)


def run_all_forecasts(config):
    # Ensure the output directory is empty.
    clean_output_dir(config['settings']['output_dir'])

    toml_files = config['settings']['toml_files']

    # Identify the forecast dates.
    forecast_dates = config['settings']['data_dates']

    output_hdf5_file = os.path.join(
        config['settings']['output_dir'],
        config['settings']['output_file'])
    if os.path.isfile(output_hdf5_file):
        print('{:%H:%M:%S} Deleting {} ...'.format(
            datetime.datetime.now(), output_hdf5_file))
        os.remove(output_hdf5_file)

    for date_str in forecast_dates:
        print('{:%H:%M:%S} Data date is {}'.format(
            datetime.datetime.now(), date_str))

        # Copy the appropriate data files to the data directory.
        populate_input_data(config['settings']['data_root_dir'], date_str)

        # Generate the forecasts for each jurisdiction.
        run_forecasts(toml_files)

        # Find the forecast output files.
        fs_files = find_output_files(config)

        # Save the observations table and forecast credible intervals table.
        save_key_tables(date_str, fs_files,
                        output_file=output_hdf5_file)

        # Remove the forecast files unless instructed to keep them.
        if not config['settings']['keep_forecast_files']:
            clean_output_dir(config['settings']['output_dir'])

        # Remove date-specific input files from the root data directory.
        remove_input_data_files(config)


def populate_input_data(data_root_dir, date_str, verbose=True):
    """
    The R_eff sample trajectories and daily case count data files for each
    data date and saved in compressed archives.
    To populate the input data for a given date, we need to identify the
    appropriate archive and extract its contents.
    """
    archive_file = os.path.join(
        'archive',
        'input_{}.tar.bz2'.format(date_str))
    print('{:%H:%M:%S} Extracting {} ...'.format(
        datetime.datetime.now(), archive_file))
    with tarfile.open(archive_file, 'r:bz2') as tar:
        tar.extractall()


def remove_input_data_files(config, verbose=True):
    data_dir = config['settings']['data_root_dir']

    reff_pattern = os.path.join(data_dir, 'reff-*.ssv')
    case_pattern = os.path.join(data_dir, 'daily-covid-cases-*.ssv')
    input_files = sorted(glob.glob(reff_pattern) + glob.glob(case_pattern))

    for f in input_files:
        if verbose:
            print('Deleting {} ...'.format(f))
        os.remove(f)


def run_forecasts(toml_files):
    num_cpus = multiprocessing.cpu_count()
    if num_cpus >= 8:
        # Run each forecast in parallel if there are sufficient CPUs.
        forecast_args = ['--spawn', str(num_cpus)]
    else:
        forecast_args = []
    for toml_file in toml_files:
        forecast_args.append(toml_file)
    # NOTE: ensure the forecasts completed successfully.
    exit_code = epifx.cmd.forecast.main(args=forecast_args)
    if exit_code != 0:
        raise ValueError('epifx-forecast returned {}'.format(exit_code))


def find_output_files(config):
    output_dir = config['settings']['output_dir']
    jurisdictions = config['settings']['jurisdictions']
    fs_files = find_forecasts(jurisdictions, data_dir=output_dir)
    return fs_files


def save_key_tables(date_str, forecast_files, output_file):
    dataset_kwargs = {
        'track_times': False,
        'track_order': False,
        'compression': 'gzip',
        'shuffle': True,
        'fletcher32': True,
    }

    for jurisdiction in sorted(forecast_files.keys()):
        flags_table = forecast_files[jurisdiction]
        for (flags, forecast_file) in flags_table.items():
            if flags == '':
                flags = 'default'
            group_name = '{}/{}/{}'.format(jurisdiction, flags, date_str)

            print('{:%H:%M:%S} Saving tables to {} ...'.format(
                datetime.datetime.now(), group_name))

            with h5py.File(forecast_file, 'r') as f:
                tbl_fs = f['/tables/forecasts'][()]
                tbl_sim = f['/tables/sim_obs'][()]
                tbl_obs = f['/observations/Notifications'][()]

            # Discard duplicate now-cast rows from the forecast table, which
            # are recorded by both the estimation pass and the forecast pass.
            fs_mask = np.logical_or(
                tbl_fs['fs_date'] == tbl_fs['fs_date'][0],
                tbl_fs['date'] != tbl_fs['fs_date'][0])
            tbl_fs = tbl_fs[fs_mask]
            tbl_fs = tbl_fs[['date', 'prob', 'ymin', 'ymax']]

            # Sort forecast credible intervals by date.
            ixs = np.argsort(tbl_fs[['date']], kind='stable', order=['date'])
            tbl_fs = tbl_fs[ixs]

            # Only retain simulated observations from the forecast pass,
            # and from the week prior to the data date.
            data_date = datetime.datetime.strptime(
                date_str, '%Y-%m-%d').date()
            date_fmt = '%Y-%m-%d %H:%M:%S'
            # Discard the final rows, which contain repeated values for the
            # forecasting date.
            tbl_sim = tbl_sim[:-2000]
            sim_dates = np.array([
                datetime.datetime.strptime(bs.decode(), date_fmt).date()
                for bs in tbl_sim['date']])
            sim_mask = (
                (sim_dates >= data_date - datetime.timedelta(days=7))
                & (sim_dates <= data_date + datetime.timedelta(days=28)))

            # Ensure we have the expected number of observations.
            expected_dates = 36
            num_samples = 2000
            expected_sum = expected_dates * num_samples
            if sum(sim_mask) != expected_sum:
                raise ValueError('Expected {} simulated obs, not {}'.format(
                    expected_sum, sum(sim_mask)
                ))
            num_dates = len(np.unique(tbl_sim['date'][sim_mask]))
            if num_dates != expected_dates:
                raise ValueError('Expected {} dates, not {}'.format(
                    expected_dates, num_dates))
            tbl_sim = tbl_sim[sim_mask]
            tbl_sim = tbl_sim[['date', 'value']]

            with h5py.File(output_file, 'a') as f:
                grp = f.create_group(group_name, track_order=False)
                grp.create_dataset('forecasts', data=tbl_fs, **dataset_kwargs)
                grp.create_dataset('obs', data=tbl_obs, **dataset_kwargs)
                grp.create_dataset('sim_obs', data=tbl_sim, **dataset_kwargs)


def find_forecasts(jurisdictions, data_dir, date_str=None, only_flags=None):
    """
    Find the most recent forecast output file for each of the specified
    jurisdictions, and ensure that each file corresponds to the same forecast
    date.

    :param jurisdictions: A list of forecast scenario IDs.
    :param data_dir: The directory in which to search.
    :param date_str: An optional forecasting date ("YYYY-MM-DD"), used to
        select forecasts other than the most recent ones.
    :param only_flags: An optional list of flags to include.

    :return: A tuple of the form ``(forecast_dates, forecast_files)`` where:

        - ``forecast_date`` is a dictionary that maps flag strings to the
          forecast date (``datetime.date``); and
        - ``forecast_files`` is a dictionary that maps jurisdiction names to
          dictionaries that map flag strings to output file names.
    """
    forecast_files = {}

    # Find all forecast output files.
    if date_str is None:
        regexp = re.compile(r'(.+)_FS-(\d{4}-\d{2}-\d{2})-.+\.hdf5')
    else:
        regexp = re.compile(r'(.+)_FS-({})-.+\.hdf5'.format(date_str))
    candidate_matches = [regexp.match(f) for f in os.listdir(data_dir)
                         if regexp.match(f)]

    for m in candidate_matches:
        jurisdiction = m.group(1)

        # NOTE: Allow this to match "JURISDICTION_some_flags".
        flags = ''
        underscore_ix = jurisdiction.find('_')
        if underscore_ix > 0:
            flags = jurisdiction[underscore_ix + 1:]
            jurisdiction = jurisdiction[:underscore_ix]

        if jurisdiction not in jurisdictions:
            # Ignore files for non-matching jurisdictions.
            continue

        if only_flags is not None and flags not in only_flags:
            # Ignore files for non-matching flags.
            continue

        input_file = m.group(0)
        forecast_date = datetime.datetime.strptime(m.group(2),
                                                   '%Y-%m-%d').date()

        # Determine whether this forecast is newer than any of the files that
        # have been seen so far.
        this_forecast = (forecast_date, input_file)
        if jurisdiction not in forecast_files:
            forecast_files[jurisdiction] = {flags: this_forecast}
        elif flags not in forecast_files[jurisdiction]:
            forecast_files[jurisdiction][flags] = this_forecast
        else:
            (prev_date, prev_file) = forecast_files[jurisdiction][flags]
            if forecast_date > prev_date:
                forecast_files[jurisdiction][flags] = this_forecast

    # Ensure that we have a forecast file for each jurisdiction.
    for jurisdiction in jurisdictions:
        if jurisdiction not in forecast_files:
            raise ValueError('No forecasts found for {}'.format(jurisdiction))

    # Remove the forecast date from each item in forecast_files.
    forecast_files = {
        jurisdiction: {flag: os.path.join(data_dir, item[1])
                       for (flag, item) in flag_table.items()}
        for (jurisdiction, flag_table) in forecast_files.items()}

    return forecast_files


if __name__ == "__main__":
    sys.exit(main())
