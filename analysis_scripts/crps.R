calculate_scores <- function(df_sim, df_truth) {
    sim_and_truth <- inner_join(df_sim, df_truth,
                                by = c('onset_date', 'state'))
    sim_columns_mask <- grepl('^sim[0-9]+$', names(sim_and_truth))

    crps_values <- crps_sample(
        sim_and_truth$cases,
        as.matrix(sim_and_truth[, sim_columns_mask]),
        method = 'edf')

    df_crps <- sim_and_truth[, ! sim_columns_mask] %>%
        ungroup() %>%
        mutate(crps_value = crps_values,
               lead_time = as.integer(onset_date - data_date),
               true_cases = cases) %>%
        select(model_id, data_date, state, onset_date, lead_time,
               true_cases, crps_value) %>%
        filter(lead_time >= -7 & lead_time <= 28) %>%
        filter(data_date >= as.Date('2020-05-01'))

    df_crps
}
